# Django crud api for users

### How to run

```
docker-compose up --build
```
Create super user
```
docker exec -it *container_id* python manage.py createsuperuser
```
And create super user

```
docker exec -it container_id python manage.py createsuperuser0
```
### Structure

```
|HTTP Method | CRUD Method | Result| Description |
| --- | --- | --- | ---
|`users` | GET | READ | Get all users
|`users/:id` | GET | READ | Get a single user
|`users`| POST | CREATE | Create a new user|
|`users/:id` | PUT | UPDATE | Update a user|
|`users/:id` | DELETE | DELETE | Delete a user|
```
### Auto generated documentation
api/schema/swagger-ui/
```
Or
```
api/schema/redoc/
```

